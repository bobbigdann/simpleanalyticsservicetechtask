﻿using System.Text;
using System.Threading;
using Cysharp.Threading.Tasks;
using Jsons;
using Newtonsoft.Json;
using UnityEngine.Networking;
using Web.Common;
using Web.Interfaces;

namespace Web.UnityRequest
{
    public sealed class WebRequestBuilder<T> : IHttpBuilder<T> where T : class
    {
        private readonly UnityWebRequest _unityWebRequest = new();
        
        private CancellationToken _cancellationToken;
        private string _body;

        public IHttpBuilder<T> SetUrl(string url)
        {
            _unityWebRequest.url = url;
            return this;
        }

        public IHttpBuilder<T> SetMethod(string method)
        {
            _unityWebRequest.method = method;
            return this;
        }

        public IHttpBuilder<T> SetBodyRaw(string body)
        {
            _unityWebRequest.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(body))
            {
                contentType = "application/json"
            };
            return this;
        }

        public IHttpBuilder<T> SetCancellationToken(CancellationToken token)
        {
            _cancellationToken = token;
            return this;
        }

        public async UniTask<HttpResponse<T>> SendAsync()
        {
            var request = await _unityWebRequest.SendWebRequest().WithCancellation(_cancellationToken);

            _cancellationToken.ThrowIfCancellationRequested();
            
            var model = typeof(T) == EmptyJson.Type
                ? EmptyJson.Default as T
                : JsonConvert.DeserializeObject<T>(request.downloadHandler.text);

            return new HttpResponse<T>(request.responseCode, model);
        }
    }
}