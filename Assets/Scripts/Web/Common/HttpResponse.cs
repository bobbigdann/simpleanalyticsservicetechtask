﻿namespace Web.Common
{
    public struct HttpResponse<T>
    {
        public long StatusCode { get; }
        public T Model { get; private set; }

        public HttpResponse(long statusCode, T model)
        {
            StatusCode = statusCode;
            Model = model;
        }
    }
}