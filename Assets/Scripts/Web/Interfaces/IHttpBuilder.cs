﻿using System.Threading;
using Cysharp.Threading.Tasks;
using Web.Common;

namespace Web.Interfaces
{
    public interface IHttpBuilder<T> where T : class
    {
        IHttpBuilder<T> SetUrl(string url);
        IHttpBuilder<T> SetMethod(string method);
        IHttpBuilder<T> SetBodyRaw(string body);
        IHttpBuilder<T> SetCancellationToken(CancellationToken token);
        UniTask<HttpResponse<T>> SendAsync();
    }
}