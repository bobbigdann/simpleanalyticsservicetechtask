﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Constants;
using Cysharp.Threading.Tasks;
using Jsons;
using Jsons.Events;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using Web.UnityRequest;
using EventType = Enums.EventType;
using Timer = Timers.Timer;

namespace Service
{
    public sealed class EventService : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private string _urlAddress;
        [SerializeField] private float _cooldownBeforeSend = 5f;
        
        private EventsContainer _eventsContainer = new();

        private void Start()
        {
            _eventsContainer = JsonConvert.DeserializeObject<EventsContainer>
            (
                PlayerPrefs.GetString(PlayerPrefsConstants.EventsData, string.Empty)
            );

            new Timer(_cooldownBeforeSend, () =>
            {
                SendEvents().Forget();
            }, true, this.GetCancellationTokenOnDestroy());
        }

        [Obsolete("Use TrackEvent(EventBase) to send events. This Function is used to send events of the same type.")]
        public void TrackEvent(string type, string data)
        {
            if (Enum.TryParse(type, out EventType typeEnum))
            {
                TrackEvent(new EventSimple()
                {
                    Type = typeEnum,
                    Data = data
                });
            }
            else
            {
                Debug.LogError($"[EventService] Incorrect type {type} to send an Event. Add this name to {typeof(EventType)}");
            }
        }
        
        public void TrackEvent(EventBase eventBase)
        {
            _eventsContainer.Events.Add(eventBase);
        }

        private async UniTask SendEvents()
        {
            var events = new List<EventBase>(_eventsContainer.Events);
            
            var response = await new WebRequestBuilder<EmptyJson>()
                .SetUrl(_urlAddress)
                .SetMethod(UnityWebRequest.kHttpVerbPOST)
                .SetBodyRaw(JsonConvert.SerializeObject(_eventsContainer))
                .SetCancellationToken(this.GetCancellationTokenOnDestroy())
                .SendAsync();

            if (response.StatusCode == (long) HttpStatusCode.OK)
            {
                _eventsContainer.Events = _eventsContainer.Events.Except(events).ToList();
            }
            else
            {
                Debug.LogError("[EventService][SendEvents] Failed to send Events to Server");
            }
        }

        private void OnApplicationQuit()
        {
            PlayerPrefs.SetString(PlayerPrefsConstants.EventsData, JsonConvert.SerializeObject(_eventsContainer));
            PlayerPrefs.Save();
        }
    }
}