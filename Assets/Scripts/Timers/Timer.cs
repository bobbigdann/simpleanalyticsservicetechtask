﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;

namespace Timers
{
    public sealed class Timer
    {
        private readonly CancellationToken _token;
        
        public Timer(float duration, Action callback, bool isLoop, CancellationToken token = default)
        {
            _token = token;
            Wait(duration, callback, isLoop).Forget();
        }

        private async UniTask Wait(float duration, Action callback, bool isLoop)
        {
            do
            {
                await UniTask.Delay(TimeSpan.FromSeconds(duration), false, PlayerLoopTiming.Update, _token);
                if (_token.IsCancellationRequested) return;
                
                callback?.Invoke();
            } while (isLoop);
        }
    }
}