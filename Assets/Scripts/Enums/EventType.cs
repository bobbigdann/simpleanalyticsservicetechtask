﻿using System.Runtime.Serialization;

namespace Enums
{
    public enum EventType : short
    {
        [EnumMember(Value = "level_start")] LevelStart = 0,
        [EnumMember(Value = "get_reward")] GetReward = 1,
        [EnumMember(Value = "spend_coins")] SpendCoins = 2,
    }
}