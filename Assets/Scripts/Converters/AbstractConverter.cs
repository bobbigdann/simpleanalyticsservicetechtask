﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Converters
{
    public class AbstractConverter<T> : JsonConverter
    {
        private readonly Dictionary<string, Type> _types;

        public AbstractConverter()
        {
            var type = typeof(T);
            _types = type.Assembly
                .GetTypes()
                .Where(t => t.IsSubclassOf(type))
                .ToDictionary(item => item.Name, item => item);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var obj = JObject.FromObject(value);
            obj.AddFirst(new JProperty("_t", value.GetType().Name));
            obj.WriteTo(writer);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var obj = JObject.Load(reader);
            var typeName = (string)obj["_t"];
            obj.Remove("_t");

            if (_types.TryGetValue(typeName, out var type))
                return (T)JsonConvert.DeserializeObject(obj.ToString(), type);

            return default;
        }

        public override bool CanConvert(Type objectType) => _types.ContainsKey(objectType.Name);
    }

}