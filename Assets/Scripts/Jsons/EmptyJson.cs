﻿using System;
using Newtonsoft.Json;

namespace Jsons
{
    [JsonObject]
    public class EmptyJson
    {
        public static readonly EmptyJson Default = new();
        public static readonly Type Type = typeof(EmptyJson);
    }
}