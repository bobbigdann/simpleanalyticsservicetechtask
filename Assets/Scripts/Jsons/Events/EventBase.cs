﻿using Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Jsons.Events
{
    [JsonObject]
    public abstract class EventBase
    {
        [JsonProperty("type"), JsonConverter(typeof(StringEnumConverter))] 
        public EventType Type;
    }
}