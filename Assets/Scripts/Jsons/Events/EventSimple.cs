﻿using Newtonsoft.Json;

namespace Jsons.Events
{
    [JsonObject]
    public class EventSimple : EventBase
    {
        [JsonProperty("data")] 
        public string Data;
    }
}