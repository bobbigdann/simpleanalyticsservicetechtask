﻿using System.Collections.Generic;
using Converters;
using Newtonsoft.Json;

namespace Jsons.Events
{
    [JsonObject]
    public class EventsContainer
    {
        [JsonProperty("events", ItemConverterType = typeof(AbstractConverter<EventBase>))] 
        public List<EventBase> Events = new();
    }
}